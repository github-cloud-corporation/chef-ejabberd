require_relative 'spec_helper'

describe 'ejabberd::default' do
  subject { ChefSpec::Runner.new.converge(described_recipe) }
  it 'installs ejabberd' do
    expect(subject).to upgrade_package('ejabberd')
  end
end
